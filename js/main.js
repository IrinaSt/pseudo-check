function pseudoCheck(pseudo, rightAnswer) {
    let userAnswer = prompt(pseudo + " это псевдокласс (введи 0) или псевдоэлемент (введи 1)? ", "");
    let userResult;
    if (Number(userAnswer) === Number(rightAnswer)) {
       userResult = 1;
    } else {
        userResult = 0;
    }
    return Number(userResult);
};

function startCheck() {
    let userAnswer1 = pseudoCheck(':before', 1);
    let userAnswer2 = pseudoCheck(':hover', 0);
    let userAnswer3 = pseudoCheck(':after', 1);
    let counter = userAnswer1 + userAnswer2 + userAnswer3;
    console.log(counter);
    if (counter === 0) {
        document.getElementById('ifBadCount').style.opacity = "1";
        return;
    } else {
        document.getElementById('checkResult').innerText = counter + " правильных ответа";
    }
};